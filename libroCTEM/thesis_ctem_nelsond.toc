\select@language {spanish}
\select@language {spanish}
\contentsline {chapter}{Contenido}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Lista de Figuras}{\es@scroman {vii}}{chapter*.2}
\contentsline {chapter}{Lista de Cuadros}{\es@scroman {viii}}{chapter*.3}
\contentsline {chapter}{Lista de S\'imbolos}{\es@scroman {ix}}{section*.4}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Antecedentes y Relevancia}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos generales y espec\IeC {\'\i }ficos}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Organizaci\IeC {\'o}n del trabajo}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Marco te\IeC {\'o}rico}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Im\IeC {\'a}genes a color}{3}{section.2.1}
\contentsline {subsubsection}{\numberline {2.1.0.1}Histograma de un canal}{4}{subsubsection.2.1.0.1}
\contentsline {section}{\numberline {2.2}Textura}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Caracterizaci\IeC {\'o}n y clasificaci\IeC {\'o}n de texturas}{5}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Morfolog\IeC {\'\i }a Matem\IeC {\'a}tica en escala de grises y color }{7}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Ordenamiento Vectorial}{9}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Resumen}{11}{section.2.4}
\contentsline {chapter}{\numberline {3}Series Morfol\IeC {\'o}gicas}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Series Morfol\IeC {\'o}gicas en escala grises}{13}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Granulometr\IeC {\'\i }a }{13}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Covarianza Morfol\IeC {\'o}gica }{15}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2} Series Morfol\IeC {\'o}gicas extendidas a color }{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Medidas de evaluaci\IeC {\'o}n}{21}{section.3.3}
\contentsline {section}{\numberline {3.4}Resumen}{23}{section.3.4}
\contentsline {chapter}{\numberline {4}Propuesta}{24}{chapter.4}
\contentsline {section}{\numberline {4.1}Distancias Intercanales}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Proporci\IeC {\'o}n Intercanal}{27}{section.4.2}
\contentsline {section}{\numberline {4.3}Volumen-RG}{28}{section.4.3}
\contentsline {section}{\numberline {4.4}Resumen}{29}{section.4.4}
\contentsline {chapter}{\numberline {5}Resultados experimentales}{30}{chapter.5}
\contentsline {section}{\numberline {5.1}Experimento 1}{30}{section.5.1}
\contentsline {section}{\numberline {5.2}Experimento 2}{33}{section.5.2}
\contentsline {section}{\numberline {5.3}Experimento 3}{36}{section.5.3}
\contentsline {chapter}{\numberline {6}Conclusiones y trabajos futuros}{41}{chapter.6}
\contentsline {section}{\numberline {6.1}Conclusiones}{41}{section.6.1}
\contentsline {section}{\numberline {6.2}Trabajos Futuros}{42}{section.6.2}
\contentsline {chapter}{Ap\IeC {\'e}ndice A}{43}{appendix*.25}
\contentsline {section}{\numberline {A.1}K-Nearest Neighbour}{43}{section.Alph0.1}
\contentsline {chapter}{Ap\'endice B}{46}{appendix*.27}
\contentsline {section}{\numberline {A.2}Resultados de clasificaci\IeC {\'o}n por clase de textura}{46}{section.Alph0.2}
\contentsline {chapter}{Ap\IeC {\'e}ndice C}{51}{appendix*.29}
\contentsline {section}{\numberline {A.3}Pruebas de segmentaci\IeC {\'o}n}{51}{section.Alph0.3}
\contentsline {chapter}{Referencias}{53}{appendix*.33}
