
\chapter{Series Morfológicas} % Series Morfologicas y medidas de evaluacion
\label{chap:seriesmorf}
La mayoría de las caracterizadores morfológicos de textura están basadas en las series morfológicas. Las series morfológicas conducen a distribuciones unidimensionales o multidimensionales basadas en una o más propiedades del elemento estructurante como el tamaño, forma, orientación \cite{aptulefe2011,lefevre2009beyond}.\\
En esta sección se presentan las definiciones de Series Morfológicas primeramente en escala de grises y luego su extensión en imágenes color.  

\section{Series Morfológicas en escala grises}
%Aplicando un filtro morfológico $\psi$ (erosión, dilatación, apertura, entre otros ) con un elemento estructurante $b$ de tamaño crecience $ \lambda$ y escribiendo $\psi_{\lambda}$ para abreviar  $\psi_{b\lambda}$\\ 

Se puede formular una serie morfológica $\textstyle \prod^{\psi}$ como un conjunto de elementos que son imágenes resultantes de aplicar sucesivamente el filtro morfológico $\psi$ a una imagen de entrada $f$, utilizando el elemento estructurante $b$ cuyo tamaño u otra propiedad $\lambda$ se varía hasta llegar a $n$:
%ecuacion principal de serie morfologica 1 
\begin{equation}
\textstyle \prod^{\psi}(f) = \left\{ {\textstyle \prod_{\lambda}^{\psi}(f) } \mid {\textstyle \prod_{\lambda}^{\psi}(f) }  = { \psi_{\lambda b}(f) }   \right\}_{0 \leq \lambda \leq n }
\end{equation}

Para $\lambda=0$, $ \psi_{0 b}(f)=f $ implica que el primer elemento es la imagen original, entonces es el tamaño de la serie está dado por $n+1$.\\
El tipo de filtro morfológico aplicado $\psi$ (dilatación, erosión, apertura, cierre, entre otros) y las características del elemento estructurante permiten generar series morfológicas que extraen características específicas de la imagen \cite{aptoula2007morphological}. 

La \textit{Covarianza Morfológica} y la \textit{Granulometría} son los principales tipos de series morfológicas. Las soluciones del enfoque morfológico que se han desarrollado usualmente son extensiones de estas series \cite{aptulefe2011}. En la Figura \ref{fig:seriesmorfologicas} se observa el esquema de una serie morfológica basada en aperturas $\gamma$ con un elemento estructurante $b$ que crece según el parámetro $\lambda$.
%En la siguiente sección se explica este tipo se serie llamada granulometría. 
\begin{figure}
    \centering
        \includegraphics[scale=1]{./img/diag_serie_granulometrica}
        \caption{Serie de Morfológica basada en aperturas con $n+1$ elementos }
        \label{fig:seriesmorfologicas}
  \end{figure} 

\subsection{Granulometría }
\label{subsec_granulometrica}
 La granulometría  es una herramienta de análisis morfológico propuesto en \cite{matheron1975random}. Es aplicado a un amplio rango de tareas especialmente en la extracción de características y la estimación de tamaño \cite{vincent2000granulometries,soille2013morphological}.\\
La granulometría $\Omega$ consiste en una familia de aperturas $\gamma_\lambda(f)$ (ecuación \ref{eq:aperturaf}) parametrizadas por un tamaño de elemento estructurante  $\lambda$ creciente, $0 \geq \lambda \leq n $, el tamaño de la serie está dado por $n+1$, considerando la imagen original. Los valores son recolectados por una medida de evaluación, generalmente el volumen ($\mathrm{Vol}$). La granulometría induce a una distribución de tamaños \cite{serra1992overview}.
La antigranulometría consiste en la utilización de cierres $\varphi_\lambda(f)$ en lugar de aperturas.

La granulometría definida como una serie morfológica está denotada por:
\begin{equation}\label{eq:seriegranul}
\Omega ^{\gamma}(f) = \left\{ {\Omega_{\lambda}^{\gamma}(f) } \mid {\Omega_{\lambda}^{\gamma}(f) } = \sum_{p \in E}^{}\textstyle \prod_{\lambda}^{\gamma} (f)(p)    \right\}_{0 \leq \lambda \leq n }
\end{equation}

En este trabajo se utilizará una notación simplificada y normalizada de $\Omega^{\gamma}(f)$ utilizada en \cite{aptoula2007morphological}, denotada por: 
\begin{equation} \label{eq:granul_gris}
G^{n}(f)= \mathrm{Vol}(\gamma_{\lambda b}(f) ) / \mathrm{Vol}(f) 
\end{equation}
Donde $n$ es el número de incrementos del elemento estructurante $b$. La normalización con el volumen de la imagen original de $f$, permite la invarianza al tamaño.

El volumen de $f$ está definido como:
\begin{equation} \label{eq:volumen}
\mathrm{Vol}(f) = \sum_{p \in E} f(p) 
\end{equation}

La familia de elementos estructurantes generados son homotéticos a un elemento estructurante convexo y compacto. Es decir el elemento estructurante debe ser compatible con la magnificación \cite{matheron1975random}.  

Como herramienta de extracción de características, la granulometría provee información del tamaño y forma de la texturas fuertemente ordenadas y la regularidad de las texturas desordenadas \cite{batman1997size}.\\  

\subsection{Covarianza Morfológica }

La covarianza morfológica propuesta en \cite{matheron1975random,maragos1989pattern} denotada por $K$ de una imagen $f$, está definida como el volumen de la imagen luego de aplicarse la erosión $\varepsilon$ a partir de un par de puntos $(x,y)$ y  $(x',y')$ separados por un vector $\overrightarrow{v}$ denotado por $P_{2,\overrightarrow{v}}$.\\

La covarianza morfológica $K$ definida como una serie morfológica está dada por :
\begin{equation}
K ^{ \overrightarrow{ v } }(f) = \left\{ {K_{\lambda}^{ \overrightarrow{ v } }(f) } \mid {K_{\lambda}^{ \overrightarrow{ v } }(f) } = \sum_{p \in E}^{}\textstyle \prod_{\lambda, \overrightarrow{ v } }^{\varepsilon} (f)(p)    \right\}_{0 \leq \lambda \leq n }
\end{equation}

En la práctica $K $ es calculado aplicando la erosión $\varepsilon$ a la imagen original $f$ con el elemento estructurante $P_{2,\overrightarrow{v}}$ variando orientaciones y longitudes de $\overrightarrow{v}$, donde $n$ es el número de variaciones de $\overrightarrow{v}$. Su versión normalizada está dada por :

\begin{equation} \label{eq:covmorf_gris}
K^n(f)  = \mathrm{Vol}(\varepsilon_{P_{2,\overrightarrow{v}}}( f )) / \mathrm{Vol}(f)
\end{equation}
La covarianza morfológica permite la detección y cuantificación de primitivas localizadas en un conjunto de distancias.
Provee información de la periodicidad, orientación y rugosidad por medio de distribuciones de orientación y distancia \cite{aptoula2007morphological}.\\

En la Figura \ref{fig:elementosestruc} se pueden observar ejemplos de elementos estructurantes. En la Figura \ref{fig:elementosestruc_1} se observa un elemento estructurante de forma cuadrada cuyo lado varía según el parámetro $\lambda$ donde el lado está dado por $2\lambda+1$. En la Figura \ref{fig:elementosestruc_2} se observan elementos estructurantes utilizados en la covarianza morfológica, cada uno compuesto por un par de puntos separados por un vector, donde varían la orientación y la distancia.    

\begin{figure}
\begin{center}
\begin{subfigure}[b]{0.9\textwidth}
        \includegraphics[scale=0.50]{img/ee/diag_grillaSE.pdf}
        \caption{Elementos estructurantes de forma cuadrada de lado $2\lambda+1$. }
        \label{fig:elementosestruc_1}
  \end{subfigure}
  \\
 
\begin{subfigure}[b]{0.9\textwidth}
        \includegraphics[scale=0.50]{img/ee/diag_SEcovmorf.pdf}
        \caption{Pares de píxeles con orientaciones y distancias variantes. }
        \label{fig:elementosestruc_2}
        
  \end{subfigure}
  \end{center}
       \caption{Elementos estructurantes utilizados en (a) la granulometría , (b) la covarianza morfológica. }
       \label{fig:elementosestruc}
\end{figure}

\section{ Series Morfológicas extendidas a color }
%estableciendo una relación de orden para dar soporte a los operadores morfológicos
Según \cite{lefevre2009beyond} si el tipo de procesamiento es vectorial, una serie morfológica para imagen a color $\mathbf{f}$ con $c$ canales queda denotada por:
\begin{equation}
\textstyle \prod^{\psi}(\mathbf{f}) = \left\{ {\textstyle \prod_{\lambda, \omega }^{\psi}(\mathbf{f}) } |  {\textstyle \prod_{\lambda, \omega }^{\psi}(\mathbf{f}) } = [{ \psi^{v}_{\lambda b} (\mathbf{f}) }]_{\omega}   \right\}_{  { 1 \leq \omega \leq c } \atop  {0 \leq \lambda \leq n} }
\end{equation}

La aplicación del filtro morfológico vectorial $\psi$ con el elemento estructurante $b$ expresada por ${ \psi^{v}_{\lambda b} (\mathbf{f}) }$ produce como resultado una nueva imagen a color.\\ 
Posteriormente se considera cada canal de manera independiente ${[ \psi^{v}_\lambda (\mathbf{f})] }_\omega$ para calcular el volumen u otra medida de evaluación.\\

Si el tipo de procesamiento es marginal, el filtro morfológico $\psi$ es aplicado para cada canal de manera independiente, entonces la serie queda definida como:
\begin{equation} \label{eq:seriemorfomarginal}
\textstyle \prod^{\psi}(\mathbf{f}) = \left\{ {\textstyle \prod_{\lambda, \omega }^{\psi}(\mathbf{f}) } |  {\textstyle \prod_{\lambda, \omega }^{\psi}(\mathbf{f}) } = { \psi_{\lambda b} (f_\omega) }   \right\}_{  { 1 \leq \omega \leq c } \atop  {0 \leq \lambda \leq n} }
\end{equation}
Donde $f_\omega$ es el plano correspondiente al $\omega$-ésimo canal de una imagen a color $\mathbf{f} = \{f_\omega \}_{ 1 \leq \omega \leq c }$

La versión vectorial de la granulometría para una imagen a color $\vec{f}$ está denotada por:
\begin{equation} \label{eq:covarcolor}
G^{n}(\mathbf{f}) = \mathrm{Vol}({[ \gamma^{v}_\lambda (\mathbf{f})] }_\omega) /  \mathrm{Vol}(f_\omega) _{ 1 \leq \omega \leq c } 
\end{equation}

La versión marginal de la granulometría está dada por la ecuación \ref{eq:granul_gris} aplicada para cada canal de manera independiente.\\

La Covarianza Morfológica en su versión vectorial para imágenes a color $\vec{f}$ se define como:
\begin{equation} \label{eq:granulcolor}
K^{n}(\mathbf{f}) = \mathrm{Vol}({[ \varepsilon^{v}_{P_{2,\vec{v}}}(\mathbf{f})] }_\omega) /  \mathrm{Vol}(f_\omega) _{ 1 \leq \omega \leq c } 
\end{equation}
En su versión marginal se aplica la ecuación \ref{eq:covmorf_gris} a cada canal.\\

En la Figura \ref{fig:ejemp_granul_000242} se pueden observar las imágenes resultantes de la aplicación de la apertura $\gamma$ para un elemento estructurante creciente. Estas imágenes constituyen los elementos que permiten obtener la Granulometría de la muestra 000242 de OutexTC13. Para cada de elemento resultante es calculado el volúmen de cada canal y finalmente es incorporado al descriptor, se debe considerar que $\lambda=0$ indica que el primer elemento es la imagen original $\vec{f}$. En la Figura \ref{fig:000242_granul} la curva granulométrica indica el efecto de la apertura sobre los volúmenes de cada canal.\\ 
% - - - -  lista de imagenes segun covarianza 
En la Figura \ref{fig:ejemp_cov_000242} se muestran las imágenes resultantes de aplicar la erosión $\varepsilon$ con las direcciones $\theta \in$ $[0^\circ$, $45^\circ$, $90^\circ$, $135^\circ]$. Para cada dirección se utilizaron las distancias $ 1 \leq d \leq 13$  variando en pasos de 4.

Los efectos de la erosión para las direcciones $\theta$ con los ángulos $0^\circ$(Figuras \ref{fig:a0d1} al \ref{fig:a0d13}), $45^\circ$( Figuras \ref{fig:a45d1} al \ref{fig:a45d13}) y  $135^\circ$(Figuras \ref{fig:a135d1} al \ref{fig:a135d13}) con sus respectivas distancias, sufrieron mayores variaciones en relación a la imagen original, esto se debe a la aplicación de la erosión  entre pares de píxeles que pertenecen a regiones disimíles de la textura. Es decir la erosión produce grandes disminuciones de intensidad cuando el vector que separa a los píxeles no es afín a la dirección y orientación dominantes en textura.\\
Las imágenes resultantes para $\theta = 90^{\circ}$ (Figuras \ref{fig:a90d1} al \ref{fig:a90d13}) son muy similares a la imagen original. Esto significa que los valores de volúmen normalizado son altos para distancias y direcciones afines a la textura. La curva de la Figura \ref{fig:000242_covar} indica la distribución de intensidad para los valores de orientación-distancia.

%  - - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - -
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242.png}
        \caption{$\lambda=0$ }
    \end{subfigure}
    %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s3.png}
        \caption{$\lambda=3$}
    \end{subfigure}
    %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s5.png}
        \caption{$\lambda=5$}
    \end{subfigure}
    %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s7.png}
        \caption{$\lambda=7$}
    \end{subfigure}
    %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s9.png}
        \caption{$\lambda=9$}
    \end{subfigure}
      %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s11.png}
        \caption{$\lambda=11$}
    \end{subfigure}
      %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s13.png}
        \caption{$\lambda=13$}
    \end{subfigure}
      %- - - - - - - - 
    \begin{subfigure}[b]{0.30\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_granEuclid/000242_granEuclid_s15.png}
        \caption{$\lambda=15$}
    \end{subfigure}
    \caption{Serie de aperturas $\gamma$ de la muestra 000242 de Outex13, usando el ordenamiento por Norma-Euclídea, con un elemento estructurante de forma cuadrada de lado $\lambda$ variando en pasos de 2.}
    \label{fig:ejemp_granul_000242}
\end{figure}

%- - -- - --- - - - - - - - - - - - - -- - - - - - - - --
%- - -- - --- - - - - - - - - - - - - -- - - - - - - - --  
\begin{figure} 
    \centering
%    \begin{subfigure}[b]{0.2\textwidth}\centering
%        \includegraphics[width=0.6\textwidth]
%        {img/000242_covarEuclid/000242.png}
%        \caption{$\theta=0^{\circ}, d=0$}
%        \label{fig:a0d0}
%    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0000_0001.png}
        \caption{$\theta=0^{\circ}, d=1$}
        \label{fig:a0d1}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0000_0005.png}
        \caption{$\theta=0^{\circ}, d=5$}
        \label{fig:a0d5}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0000_0009.png}
        \caption{$\theta=0, d=9$}
        \label{fig:a0d9}
    \end{subfigure}
     \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0000_0013.png}
        \caption{$\theta=0^{\circ}, d=13$}
        \label{fig:a0d13}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0045_0001.png}
        \caption{$\theta=45, d=1$}
        \label{fig:a45d1}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0045_0005.png}
        \caption{$\theta=45^{\circ}, d=5$}
        \label{fig:a45d5}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0045_0009.png}
        \caption{$\theta=45^{\circ}, d=9$}
        \label{fig:a45d9}
    \end{subfigure}
     \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0045_0013.png}
        \caption{$\theta=45^{\circ}, d=13$}
        \label{fig:a45d13}
    \end{subfigure}
     \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0090_0001.png}
        \caption{$\theta=90^{\circ}, d=1$}
        \label{fig:a90d1}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0090_0005.png}
        \caption{$\theta=90^{\circ}, d=5$}
        \label{fig:a90d5}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0090_0009.png}
        \caption{$\theta=90^{\circ}, d=9$}
        \label{fig:a90d9}
    \end{subfigure}
     \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0090_0013.png}
        \caption{$\theta=90^{\circ} \circ, d=13$}
        \label{fig:a90d13}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0135_0001.png}
        \caption{$\theta=135^{\circ}, d=1$}
        \label{fig:a135d1}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0135_0005.png}
        \caption{$\theta=135^{\circ}, d=5$}
        \label{fig:a135d5}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0135_0009.png}
        \caption{$\theta=135^{\circ}, d=9$}
        \label{fig:a135d9}
    \end{subfigure}
     \begin{subfigure}[b]{0.25\textwidth}\centering
        \includegraphics[width=0.6\textwidth]
        {img/000242_covarEuclid/000242_0135_0013.png}
        \caption{$\theta=135^{\circ} \circ, d=13$}
        \label{fig:a135d13}
    \end{subfigure}
    
\caption{Serie de covarianza morfológica a color de la muestra 000242 de OutexTC13}
\label{fig:ejemp_cov_000242}
\end{figure}
%- - - - - - - - - - - - - - - - --  -
\begin{figure}
%\centered
\begin{center}
    \includegraphics[scale=0.50]{./img/000242_gran_RGB.pdf}
    \caption{ Granulometría de la muestra 000242 }
    \label{fig:000242_granul}
\end{center}
\end{figure}
% - - - - -  --  - - - - - - - - 
\begin{figure}
\begin{center}
    \includegraphics[scale=0.50]{./img/000242_cov_RGB.pdf}
    \caption{ Covarianza Morfológica de la muestra 000242 }
    \label{fig:000242_covar}
\end{center}
\end{figure}

%- -- - - - -   -  - - - - - -- - - - - - - - - - - - - - - - - - -


%\section{Medidas de Evaluación}
%Volumen Vectorial:
%Desviaci
En el Algoritmo \ref{alg:granulgris_pseudo} se observa la secuencia de pasos correspondientes al cálculo de la granulometría para una imagen en escala de grises $f$. Los parámetros de entrada son una imagen en escala de grises $f$ que representa a la muestra de textura, el tamaño de la serie $n$ y el elemento estructurante convexo $b$ que será magnificado. El algoritmo retorna un vector de características de la textura, que provee una distribución de tamaños.\\
En el Algoritmo \ref{alg:covmgris_pseudo} se observa la secuencia de pasos correspondientes al cálculo de la covarianza morfológica para $f$. Sus parámetros de entrada son una imagen de textura $f$, la lista de direcciones $O$ y las distancias de separación $D$. El algoritmo retorna un vector de característica de la textura, que provee una distribución de orientación e intensidad.  

%-- traducir a español 
\makeatletter
\renewcommand*{\ALG@name}{Algoritmo}
\makeatother


%- - - - - - - - - - - - - - - - - - - - - - -- 
\begin{algorithm}[]


\caption{Granulometría en escala de grises}\label{alg:granulgris_pseudo}
\begin{algorithmic}[1]
\Procedure{Granulometria}{$f$,$b $, $n$}
\Require
\Statex {$f$ es la imagen en escala de grises.}
\Statex{$b$ es el elemento estructurante. }
\Statex{ $n$ es el tamaño de la serie. }

\Ensure \Statex{un vector de caraterísticas $V$ de tamaño n+1, como descriptor de $f$ }


\State $V \in \mathbb{R}^{n+1} \gets  0 $ 

\For{$\lambda$=0, $\lambda \leq n+1 $ } 
%\State \Comment{APERTURA retorna una imagen filtrada con $\gamma$ \ref{}}

\State { $V[\lambda]  \gets \mathrm{Vol}(\mathrm{APERTURA} (f,\lambda b ))$ / $\mathrm{Vol}(f)$ } 

\State \Comment{ la función APERTURA está definida por la ecuación \ref{eq:aperturaf}}.

\EndFor

\State \textbf{return} $V$\Comment{ }
\EndProcedure
\end{algorithmic}
\end{algorithm}
%---------------------------------------

%- - - - - - - - -  -
\begin{algorithm}[]

\caption{ Covarianza Morfológica en escala de grises}\label{alg:covmgris_pseudo}
\begin{algorithmic}[1]
\Procedure{CovarMorf}{$f$,$D$, $O$}
\Require
\Statex {$f$ es imagen en escala de grises. }
\Statex {$D$ es un conjunto de distancias.  }
\Statex {$O$ es un conjunto de ángulos.  }
\Ensure 
\Statex{un vector de caraterísticas $V$, el descriptor de $f$ cuyo tamaño está dado por la número de elementos en $O$ por las el número de distancias en $D$.}


\State $V \in \mathbb{R}^{ ( \mathrm{card}( D )   \times \mathrm{card}(O)  )  } \gets  0 $

\ForAll{$\theta \in O $} 
\ForAll{$d \in D $} 
%--- - - 
%\State {$Pa \gets (x,y) $}
%\State {$Pb \gets (x+ (\cos(\theta)*d), y + (\sin(\theta)*d)) $}

%\State{ $|| \overrightarrow{ P_a, P_b }  ||= d $  }
%separados por un vector con el angulo $\theta$ y la distancia $d$


%\State {$ Pa,Pb \mid d={|| \overrightarrow{ Pa, Pb }  ||} \wedge  \overrightarrow{v} = \frac{\overrightarrow{ P_a, P_b }}  {|| \overrightarrow{ P_a, P_b }  ||} $    } 

%\State{ $ \frac{\overrightarrow{ P_a, P_b }}  {|| \overrightarrow{ P_a, P_b }  ||}= \overrightarrow{v} $  }
\State { $P_{2v} \gets \{P_a,P_b\} $} 


\Statex{ \Comment{el elemento estructurante $P_{2v}$ está compuesto por un par de puntos $Pa$, $Pb$. Donde $\theta$ es la dirección del vector $\overrightarrow{P_a,P_b}$ y $d$ es el módulo de dicho vector}}


% - - -- - - - 

\State { $V[\theta \times d]  \gets \mathrm{Vol}(\mathrm{EROSION}(f,P_{2v} ))$ / $\mathrm{Vol}(f)$ }

\State \Comment{la función EROSION está definida en la ecuación \ref{eq:erosionf}.  }
% - - - - -
\EndFor
\EndFor

\State \textbf{return} $V$\Comment{retorna el vector de caraterísticas, es decir el descriptor de $f$. }
\EndProcedure
\end{algorithmic}
\end{algorithm}


\section{Medidas de evaluación} \label{sec:medida_eval}
De forma general la extracción de características de la imagen $\vec{f}$ se realiza en dos pasos:
\begin{enumerate}
\item Construcción de la serie morfológica ($\textstyle \prod^{\psi}(f)$ ).
\item Recolección de valores o alguna medida que represente a cada elemento de la serie, o dicho de otra manera, el efecto que tuvo la operación morfológica $\psi$ sobre la imagen $f$. Usualmente el volumen es la medida utilizada. 
\end{enumerate}
%-------------sobre el calculo del volumen de la imagen --------------
Además del volumen, pueden ser calculados otros momentos estadísticos para formar el vector final de características como: media, varianza, asimetría, kurtosis entre otros\cite{astoladougherty1999nonlinear}.\\
En este trabajo se utilizan los momentos estadísticos que conducen a mejores resultados en \cite{aptulefe2011}. A continuación se definen las medidas varianza, energía y entropía: \\
La varianza del conjunto de los valores de intensidad de una imagen $f$ está definida como:
\begin{equation} \label{eq:varianza}
\mathrm{Varianza}(f) = \frac{1}{\mathrm{card}(E)}  \sum_{p \in E} (f(p)-\overline{ f(p) } )^2
\end{equation}  
Donde $\mathrm{card}(E)$ es el número de píxeles de la imagen $f$ y $\overline{ f(p) }$ es la media aritmética de $f(p)$

La energía de una imagen $f$ está definida como:
\begin{equation} \label{eq:energia}
\text{Energía}(f) = \sum_{p \in E} f(p)^2 
\end{equation}
% - - - - - -- - - - -
% -sum(p.*log2(p))
 
La entropía del histograma $h$ de una imagen $f$ (ecuación \ref{eq:histograma}) esta definida como:
\begin{equation} \label{eq:entropia}
\text{Entropía}(f) = -\sum_{l \in [0,2^{k}-1] } \frac{h_f(l)}{\mathrm{card}(E)}  \times \mathrm{log_2}(\frac{h_f(l)}{\mathrm{card}(E)}) 
\end{equation} 
Donde $[0,2^{k}-1]$ es el intervalo de niveles de intensidad y $k$ es el número de bits disponibles para representar un valor de intensidad.\\
Usualmente estas medidas son calculadas para cada canal de la imagen $\vec{f}$ y posteriormente son concatenadas.\\ 

El hipervolumen es una medida intercanal para un espacio de color RGB utilizada en \cite{ivanovicicolor} definida como:
\begin{equation} \label{eq:hipervolumen}
\mathrm{Hipervolumen}(\vec{f}) = \sum_{p \in E } f_1(p) \times f_2(p) \times f_3(p) 
 \end{equation} 
 Donde $f_1$ ,$f_2$, $f_3$ son los planos R,G,B respectivamente de la imagen $\vec{f}$.
%Donde $h$ es el histograma de la imagen $f$ 
 

\section{Resumen}
En este capítulo fueron presentadas las definiciones relacionadas a las series morfológicas. Las series morfológicas son generadas por la aplicación sucesiva de un filtro morfológico. Los principales tipos de series morfológicas son la granulometría y la covarianza morfológica. La granulometría utiliza elementos estructurantes convexos y crecientes. La covarianza morfológica utiliza un par de puntos separados por un vector, donde la orientación y la distancia son variantes.\\
El volumen se define como la suma de todos los valores de intensidad y es usado como medida de evaluación. El volumen permite obtener los descriptores a partir de las series morfológicas. Alternativamente pueden ser usadas otras medidas de evaluación, en este trabajo se trataron las siguientes medidas: energía, varianza, entropía e hipervolumen.\\
En el siguiente capítulo se presenta la propuesta de este trabajo, consistente en la formulación de medidas intercanales.
