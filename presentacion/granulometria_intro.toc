\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Textura}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Conceptos }{3}{0}{1}
\beamer@subsectionintoc {1}{2}{An\IeC {\'a}lisis de textura }{6}{0}{1}
\beamer@sectionintoc {2}{Aplicaciones}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Aplicaciones del an\IeC {\'a}lisis de textura}{8}{0}{2}
\beamer@sectionintoc {3}{Extracci\IeC {\'o}n de carater\IeC {\'\i }sticas y clasificaci\IeC {\'o}n}{14}{0}{3}
\beamer@subsectionintoc {3}{1}{Extracci\IeC {\'o}n de caracter\IeC {\'\i }sticas}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Clasificaci\IeC {\'o}n}{19}{0}{3}
\beamer@sectionintoc {4}{Enfoque morfol\IeC {\'o}gico }{21}{0}{4}
\beamer@subsectionintoc {4}{1}{Aspectos generales}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{Series morfol\IeC {\'o}gicas}{25}{0}{4}
\beamer@subsectionintoc {4}{3}{Distribuci\IeC {\'o}n de tama\IeC {\~n}o}{28}{0}{4}
\beamer@subsectionintoc {4}{4}{ Otras distribuciones }{40}{0}{4}
\beamer@sectionintoc {5}{Extensi\IeC {\'o}n a color }{43}{0}{5}
\beamer@sectionintoc {6}{Estado actual de las tareas }{44}{0}{6}
