\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{6}
\contentsline {section}{\numberline {1.1}Objetivos}{7}
\contentsline {subsection}{\numberline {1.1.1}Objetivo General y propuesta }{7}
\contentsline {subsection}{\numberline {1.1.2}Objetivos Espec\IeC {\'\i }ficos}{7}
\contentsline {section}{\numberline {1.2}Organizaci\IeC {\'o}n del trabajo}{7}
\contentsline {chapter}{\numberline {2}Marco Te\IeC {\'o}rico}{9}
\contentsline {section}{\numberline {2.1}Textura}{9}
\contentsline {section}{\numberline {2.2}An\IeC {\'a}lisis de textura}{11}
\contentsline {subsection}{\numberline {2.2.1}Enfoques para el An\IeC {\'a}lisis de Textura}{12}
\contentsline {section}{\numberline {2.3}Clasificaci\IeC {\'o}n y caracterizaci\IeC {\'o}n de textura}{13}
\contentsline {section}{\numberline {2.4}Matem\IeC {\'a}tica Morfol\IeC {\'o}gica}{14}
\contentsline {subsection}{\numberline {2.4.1}Nociones B\IeC {\'a}sicas}{15}
\contentsline {subsection}{\numberline {2.4.2}Ret\IeC {\'\i }culo Completo, Ordenamiento e Inclusi\IeC {\'o}n}{15}
\contentsline {subsection}{\numberline {2.4.3}Operadores morfol\IeC {\'o}gicos}{16}
\contentsline {section}{\numberline {2.5}Enfoque morfol\IeC {\'o}gico en la caracterizaci\IeC {\'o}n de textura}{17}
\contentsline {subsection}{\numberline {2.5.1}Series morfol\IeC {\'o}gicas}{19}
\contentsline {subsection}{\numberline {2.5.2}Distribuciones de tama\IeC {\~n}o Granulometr\IeC {\'\i }a y Patr\IeC {\'o}n espectro }{20}
\contentsline {subsection}{\numberline {2.5.3}Distribuciones de orientaci\IeC {\'o}n- Covarianza morfol\IeC {\'o}gica}{23}
\contentsline {subsection}{\numberline {2.5.4}Variograma}{26}
\contentsline {subsection}{\numberline {2.5.5}Distribuciones extendidas }{26}
\contentsline {subsection}{\numberline {2.5.6}Distribuciones invariantes de rotaci\IeC {\'o}n}{26}
\contentsline {subsection}{\numberline {2.5.7}Invarianza a la iluminaci\IeC {\'o}n}{26}
\contentsline {subsection}{\numberline {2.5.8}Distribuciones de tama\IeC {\~n}o y forma}{30}
\contentsline {subsection}{\numberline {2.5.9}M\IeC {\'e}todo de evaluaci\IeC {\'o}n }{31}
\contentsline {subsubsection}{Momentos invariantes}{32}
\contentsline {subsection}{\numberline {2.5.10}Elecci\IeC {\'o}n el elemento estructurante }{33}
\contentsline {section}{\numberline {2.6}Im\IeC {\'a}genes a Color}{33}
\contentsline {subsection}{\numberline {2.6.1}Espacios de Color}{34}
\contentsline {section}{\numberline {2.7}Morfolog\IeC {\'\i }a matem\IeC {\'a}tica en color o multivalorada}{36}
\contentsline {subsection}{\numberline {2.7.1}Ordenamiento de vectores}{36}
\contentsline {subsubsection}{\textbf {Ordenamiento Marginal}}{36}
\contentsline {subsubsection}{\textbf {Ordenamiento por Reducci\IeC {\'o}n}}{37}
\contentsline {subsubsection}{\textbf {Ordenamiento Condicional o Lexicogr\IeC {\'a}fico}}{37}
\contentsline {subsection}{\numberline {2.7.2}Estrategias de procesamiento }{38}
\contentsline {subsection}{\numberline {2.7.3}Impacto del m\IeC {\'e}todo de ordenamiento y el espacio}{39}
\contentsline {subsection}{\numberline {2.7.4} Granulometr\IeC {\'\i }a en im\IeC {\'a}genes multiespectrales }{40}
\contentsline {section}{\numberline {2.8}Aplicaciones}{45}
\contentsline {subsection}{\numberline {2.8.1}An\IeC {\'a}lisis de documentos }{45}
\contentsline {subsection}{\numberline {2.8.2}Im\IeC {\'a}genes Biom\IeC {\'e}dicas }{45}
\contentsline {subsection}{\numberline {2.8.3}Im\IeC {\'a}genes Remotas }{47}
\contentsline {subsection}{\numberline {2.8.4}Inspecci\IeC {\'o}n industrial}{47}
\contentsline {subsection}{\numberline {2.8.5}Geolog\IeC {\'\i }a}{48}
\contentsline {subsection}{\numberline {2.8.6}Segmentaci\IeC {\'o}n de textura}{48}
\contentsline {subsection}{\numberline {2.8.7}Recuperaci\IeC {\'o}n de im\IeC {\'a}genes basada en el contenido }{48}
\contentsline {section}{\numberline {2.9}Implementacion }{50}
\contentsline {section}{\numberline {2.10}Diagramas pendientes}{50}
\contentsline {section}{\numberline {2.11} Clasificadores }{50}
\contentsline {chapter}{\numberline {3}Experimentos y resultados }{51}
\contentsline {section}{\numberline {3.1}Outex}{51}
\contentsline {section}{\numberline {3.2}Descripci\IeC {\'o}n de pruebas}{52}
\contentsline {subsection}{\numberline {3.2.1}Experimento 2}{54}
\contentsline {subsection}{\numberline {3.2.2}Experimento 3}{56}
