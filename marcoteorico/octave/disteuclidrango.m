
%tx = ty = linspace (10, 100)';
tx = ty =  10:10:100';
[xx, yy] = meshgrid (tx, ty);
%r = sqrt (xx .^ 2 + yy .^ 2) /  + eps;
%tz = 2 * ((abs (xx  - yy )) / (xx  + yy )) ;
tz = sqrt (( (xx - yy) / (0.5 .* xx  + yy )).^ 2)  ;




mesh (tx, ty, tz, "linewidth",20);

hx = xlabel ("Vol(Q1)"); 
set (hx, "fontsize", 14);
hy = ylabel ("Vol(Q2)"); 
set (hy, "fontsize", 14);
hz = zlabel ("Distancias euclidea intercanal"); 
  
print("disteuclid.pdf", '-color', '-dpdfwrite');

