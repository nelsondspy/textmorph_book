X= [1:20];
Y =  [0.0746,0.0524,0.0503,0.0319,0.0351,0.0227,0.0196,0.0275,0.0426,0.0647,0.0129,0.0146,0.0284,0.0668,0.0803,0.0132,0.0129,0.0131,0.0147,0.0149];
#---------------------------
plot( X , Y ,  ":+k" );
xlabel(' size SE ');
ylabel('Diferencial de Vol(i-1) - Vol(i) ');
legend(' Original ', 'FontName', 'Arial');
title('Patron espectro');

set([gca; findall(gca, 'Type','text')], 'FontName', 'Arial');

print -color -dpdfwrite granul_moneda_patron.pdf	


#---------------------------
figure;

XG = [1:20];
YG = [0.9254,0.8732,0.8270,0.7954,0.7638,0.7414,0.7222,0.6984,0.6600,0.5984,0.5860,0.5718,0.5459,0.4805,0.4011,0.3884,0.3765,0.3640,0.3501,0.3359];
plot( XG, YG ,  ":+k" );
xlabel(' size SE ');
ylabel('y (Vol )');
legend(' Original ', 'FontName', 'Arial');
title(' Volumen Normalizado ');

set([gca; findall(gca, 'Type','text')], 'FontName', 'Arial');

print -color -dpdfwrite granul_monedas_granulometria.pdf	


#--------------probabilioda acumulada -----------------
Yp=[0.0746, 0.1268, 0.1730, 0.2046, 0.2362, 0.2586, 0.2778, 0.3016, 0.3400, 0.4016, 0.4140, 0.4282, 0.4541, 0.5195, 0.5989,0.6116, 0.6235, 0.6360, 0.6499,0.6641];



Xp= [1:20];

plot( Xp, Yp ,  ":+k" );
xlabel(' size SE ');
ylabel('y (Vol )');
legend(' Original ', 'FontName', 'Arial');
title('Distribucion Granulometrica ');
set([gca; findall(gca, 'Type','text')], 'FontName', 'Arial');

print -color -dpdfwrite granul_monedas_distribuciongran.pdf	

