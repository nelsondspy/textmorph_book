%funcion que dibuja la curva granulometrica RGB  
%Se asume que v es un array y p es un entero que indica la cantidad de valores por cada elemento 
% de esta manera si existen N elementos de serie y P valores para cada elemento el tamaño del array debe ser N*P   
function res  = multigraphinter ( v1, v2, p , output , titulo, labels )
    np = length ( v1 ) ;
    res1 = zeros(p, np/p );
    res2 = res1 ;
    disp("np =") , disp(np);
    disp("np/p =") ,  disp(np/p);

    i = 1 ;
    cult = 1 ;
    while(i <= np )
	for c = cult:( cult  + (p - 1) )            
	for f = 1:p
               if i > np
                  break;  
               endif 
               res1(f,c) = v1( i ) ;
	       res2(f,c) = v2( i ) ;
               i = i + 1 ;


 

        endfor  
        endfor
        cult = cult + p ;

    endwhile

%resultados de v1
r1Y1= res1(1,:);
r1Y2= res1(2,:);
r1Y3= res1(3,:);

r1X1= 1:np/p;
r1X2= r1X1 ;
r1X3 = r1X1;

%resultados de v2
r2Y1= res2(1,:);
r2Y2= res2(2,:);
r2Y3= res2(3,:);

r2X1 = r1X1;
r2X2 = r1X1;
r2X3 = r1X1;




grosorlinea = 2 ; 

set(gca, "linewidth", 4, "fontsize", 12)


plot( r1X1, r1Y1,  ":+m;Muestra1 D(R,G);" ,"linewidth",grosorlinea,...
      r1X2, r1Y2,  ":+m;Muestra1 D(G,B);" , "linewidth",grosorlinea ,... 
      r1X3, r1Y3,  ":+m;Muestra1 D(B,R);",  "linewidth",grosorlinea,...
      r2X1, r2Y1,  ":ok;Muestra2 D(R,G);"  ,"linewidth",grosorlinea,...
      r2X2, r2Y2,  ":ok;Muestra2 D(G,B);" , "linewidth",grosorlinea ,... 
      r2X3, r2Y3,  ":ok;Muestra2 D(B,R);", "linewidth",grosorlinea );


hx = xlabel (labels(1)); 
set (hx, "fontsize", 14) 

hy = ylabel(labels(2));
set (hy, "fontsize", 14) 

htitle = title( titulo );
set (htitle, "fontsize", 18) 

#legend('  ', 'FontName', 'Arial');

set([gca; findall(gca, 'Type','text')], 'FontName', 'Arial');

%print -color -dpdfwrite output
%print(output, '-color', '-dpdfwrite');
print(output, '-color', '-dsvg');
endfunction


