clear;


%IMFILE = '69040_small.jpg';
%IMFILE = '181079_small.jpg';
%IMFILE ='35070.jpg';




filename = uigetfile({'*.tif;*.jpg;*.jpge;*.png'});

[pathstr, name, ext] = fileparts(filename);

IMFILE =filename ;

vec = datevec( now );
dirname = datestr( vec, 'yyyy-mm-dd HH:MM' );
dirname = strrep(dirname , ':', '');

OUTPUTFOLDER = dirname  ;
OUTPFILE = strcat(dirname, '/', name );




%----------------------------------
%----------------------------------

he = imread( IMFILE );

M = size(he, 1);
N = size(he, 2);

imshow(he), title('H&E image');
text(size(he,2),size(he,1)+15,...
     'Image courtesy of Alan Partin, Johns Hopkins University', ...
     'FontSize',7,'HorizontalAlignment','right');

NCLUSTERS = 3 ;
DIMFEATURE = 20 ;

feat = double(zeros( M*N ,DIMFEATURE  ));
d = 10;

Pixelpos = 0 ;


for f = 1:M 
    for c = 1:N 
        
  
        f_minus_d  = f - d ;
        f_plus_d  = f + d ;
        c_minus_d = c - d;
        c_plus_d  = c + d ; 
        
        if (f-d) <= 0 
           f_minus_d = 1;  
        end
        if (f+d)> M 
            f_plus_d = M;
        end
        
        if (c-d)<= 0 
           c_minus_d = 1;  
        end
        if (c+d)> N 
            c_plus_d = N;
        end    
            
        P1 = [ f_minus_d , c_minus_d ];
        P2 = [ f_minus_d, c_plus_d ];
        P3 = [ f_plus_d , c_minus_d ];
        P4 = [ f_plus_d , c_plus_d ];
        
        %calc = zeros(P1(1)-P4(1), P1(2)-P4(2), 3 );
        
        %imshow(he(P1(1):P4(1),P1(2):P4(2),:) );    
        MUESTRA = he(P1(1):P4(1),P1(2):P4(2),:);
        
        %posicion en el vector global de caracteristicas
        
        Pixelpos = Pixelpos + 1 ;
        
        fpos=0;
        
        SIZEE = 3;
        
        %volumenes originales 
        v0r = sum(sum(MUESTRA(:,:,1))) ;
        v0g = sum(sum(MUESTRA(:,:,2))) ;
        v0b = sum(sum(MUESTRA(:,:,3))) ;
        
        %--GRANULOMETRIA Y ANTIGRANULOMETRIA
        for s = 1:SIZEE 
            se= strel('disk',s);
            %---apertura -----graulometria
            ER =  imdilate(imerode(MUESTRA,se),se);
            volr = sum( sum(ER(:,:,1)) ); 
            volg = sum( sum(ER(:,:,2)) );
            volb = sum( sum(ER(:,:,3)) );
            
            %incorporacion de caracteristicas al vector 
            fpos = fpos + 1 ;
            feat(Pixelpos, fpos ) = volr/v0r ;
            
            fpos = fpos + 1 ;
            feat(Pixelpos, fpos ) = volg/v0g ;
            
            fpos = fpos + 1;
            feat(Pixelpos, fpos ) = volb/v0b ;
            
            %--medidas intercanales 
             fpos = fpos + 1;
            feat(Pixelpos, fpos ) = volr  / (volr + volg  + volb);
            
            fpos = fpos + 1;
            feat(Pixelpos, fpos ) = volg  / (volr + volg  + volb);
            
            %---cierre-----antigranulometria
            DR =  imerode(imdilate(MUESTRA,se),se);
            volr = sum( sum(DR(:,:,1)) ); 
            volg = sum( sum(DR(:,:,2)) );
            volb = sum( sum(DR(:,:,3)) );
              %incorporacion de caracteristicas al vector 
            fpos = fpos + 1 ;
            feat(Pixelpos, fpos ) = volr/v0r ;
            
            fpos = fpos + 1 ;
            feat(Pixelpos, fpos ) = volg/v0g ;
            
            fpos = fpos + 1;
            feat(Pixelpos, fpos ) = volb/v0b ;
            
            %---medidas intercanales 
            fpos = fpos + 1;
            feat(Pixelpos, fpos ) = volr  / (volr + volg  + volb);
            
            fpos = fpos + 1;
            feat(Pixelpos, fpos ) = volg  / (volr + volg  + volb);
            
             
        end
        
       for ang = 0:45:135
        for d = 3:2:15     
            se2p = strel('line', d, ang) ;
            se_mat = getnhood(se2p) ;
            se_mat( 2:end-1, 2:end-1 ) = 0 ;
            se2p = strel('arbitrary', se_mat);
            %--resultado de la covarianza morfologica
            COVMRES = imerode(MUESTRA, se2p);
            cm_volr= sum(sum(COVMRES(:,:,1)));
            cm_volg= sum(sum(COVMRES(:,:,2)));
            cm_volb= sum(sum(COVMRES(:,:,3)));

            fpos = fpos + 1 ;
            feat(Pixelpos, fpos ) = cm_volr/v0r ;

             fpos = fpos + 1 ;
             feat(Pixelpos, fpos ) = cm_volg/v0g ;

             fpos = fpos + 1 ;
             feat(Pixelpos, fpos ) = cm_volb/v0b ;

        end
       end
        
    

        %--volumenes originales 
    
       
        disp(Pixelpos);



%         if( f==1  && c==1 )
%          
%              return ;
%        end 
        
       
        

        
    end
    
end

%


mkdir( OUTPUTFOLDER );

%--FORMAR LOS CLUSTERS 
[cluster_idx, cluster_center] = kmeans(feat,NCLUSTERS,'distance','sqEuclidean','Replicates',3);

%vector de caracteristicas
pixel_labels = reshape(cluster_idx,N,M)';
imshow(pixel_labels,[]); title('image labeled by cluster index');
h = getframe;
im = h.cdata;

imwrite(h.cdata, strcat(OUTPFILE, '_GRAY.tif'),'tif');



 segmented_images = cell(1,NCLUSTERS);
rgb_label = repmat(pixel_labels,[1 1 3]);

 for k = 1:NCLUSTERS
     color = he;
     color(rgb_label ~= k) = 0;
     segmented_images{k} = color;
     figure ; imshow(segmented_images{ k });
     imwrite(segmented_images{ k } ,strcat(OUTPFILE,'_s',num2str(k),'.tif'),'tif');

 end

%-------------------------------
colorMarc = he;
seBord= strel('square',3);

segmented_mask = cell(1,NCLUSTERS);
for k = 1:NCLUSTERS
    t1 = (pixel_labels == k);
    
    s1 = (imdilate(t1,seBord)) - t1 ;
    %s1 = s1*.255;
    segmented_mask{k} = s1;
    % figure; imshow(segmented_mask{k});
   
end

aux1 = segmented_mask{1}+segmented_mask{2}+segmented_mask{3};
auxrgb = uint8(cat(3, aux1*.0, aux1*255, aux1*.0));
figure; imshow( auxrgb + colorMarc );
 imwrite(auxrgb + colorMarc ,strcat(OUTPFILE,'_MARC.tif'),'tif');

%- - - - - - - - -  - - - - - - 


