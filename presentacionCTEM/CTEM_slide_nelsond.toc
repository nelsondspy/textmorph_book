\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Conceptos previos}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{An\IeC {\'a}lisis de Textura}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Morfolog\IeC {\'\i }a Matem\IeC {\'a}tica}{10}{0}{1}
\beamer@subsectionintoc {1}{3}{Series morfol\IeC {\'o}gicas}{16}{0}{1}
\beamer@sectionintoc {2}{Problem\IeC {\'a}tica y motivaci\IeC {\'o}n}{25}{0}{2}
\beamer@subsectionintoc {2}{1}{Objetivo General }{28}{0}{2}
\beamer@sectionintoc {3}{Propuesta}{30}{0}{3}
\beamer@sectionintoc {4}{Resultados experimentales}{39}{0}{4}
\beamer@sectionintoc {5}{Conclusiones}{44}{0}{5}
